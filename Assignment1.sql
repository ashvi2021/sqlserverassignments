create database Assignment1
use Assignment1
create table worker(WORKER_ID INT primary key,
	FIRST_NAME CHAR(25) not null,
	LAST_NAME CHAR(25),
	SALARY int check ( SALARY between 10000 and 25000),
	JOINING_DATE DATETIME,
	DEPARTMENT CHAR(25) check(DEPARTMENT in('HR', 'Accts', 'IT')))

Insert into worker values
(001,'Monika','Arora',11000, '2014-02-20','HR'),
(002,'Rita','Jain',12000, '2014-10-20','IT'),
(003,'Shyam','Sharma',13000, '2014-11-18','HR'),
(004,'Reenu','Singh',15000, '2014-05-20','IT'),
(005,'Mohit','Garg',17000, '2014-09-10','HR'),
(006,'Krishn','Gupta',18000, '2014-06-20','Accts'),
(007,'Anu','Aggarwal',19000, '2014-07-30','IT'),
(008,'Radhika','Dubey',20000, '2014-02-01','Accts'),
(009,'Ashi','Gupta',21000, '2014-09-11','IT'),
(010,'Anuj','Goel',24000, '2014-04-22','IT')


Select * from worker

--Ouestion 1-- 

select FIRST_NAME as WORKER_NAME from worker 

--Question 2 ---

select UPPER (FIRST_NAME) from worker 

--Question 3 Unique values---

select WORKER_ID from worker

--Question 4---

select SUBSTRING (FIRST_NAME, 1,3) from worker

--Question 5 ---

select CHARINDEX('a' , 'FIRST_NAME') from worker where FIRST_NAME='Radhika'


--Question 6 ---

select RTRIM ('ka') from worker where FIRST_NAME='Radhika'

select LTRIM ('Sh') from worker where FIRST_NAME='Shyam'
 

 --Question 7 ---

 select DEPARTMENT, LTRIM (DEPARTMENT) from worker

 --Question 8 ---

 select DEPARTMENT, LEN(DEPARTMENT) from worker

 --Question 9---

 select REPLACE('FIRST_NAME', 'a', 'A') from worker

 --Question 10 ---

 Select CONCAT(FIRST_NAME, ' ', LAST_NAME) AS 'COMPLETE_NAME' from worker

 --Question 11---

 select * from Worker order by FIRST_NAME asc

 --Question 12--
  
select * from Worker order by FIRST_NAME asc, DEPARTMENT desc

 --Question 13--
 select * from worker where FIRST_NAME in ('Mohit', 'Anu')

 --Question 14--

select * from worker where FIRST_NAME not in ('Mohit' , 'Anu')

--Question 15--

  select * from worker where DEPARTMENT in('HR')

--Question 16--

select * from worker where FIRST_NAME like '%a%';

--Question 17---

select * from worker where FIRST_NAME like '%a'

--Question 18--

select * from worker where FIRST_NAME like '_____a'

--Question 19--

Select * from Worker where SALARY between 11000 and 19000;

--Question 20--

Select * from Worker where year(JOINING_DATE) = 2014 and month(JOINING_DATE) = 2;

--Question 21---

select count(DEPARTMENT) from worker
